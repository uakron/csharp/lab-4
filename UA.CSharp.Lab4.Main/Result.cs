﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UA.CSharp.Lab4.Main
{
    public class Result
    {
        public string Title { get; set; }

        public string Date { get; set; }

        public string Path { get; set; }
    }
}
