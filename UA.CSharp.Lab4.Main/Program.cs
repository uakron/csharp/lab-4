﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace UA.CSharp.Lab4.Main
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Please enter an author's name: ");
            var author = Console.ReadLine();

            var validDate = false;
            DateTime startDate = default;
            while (!validDate)
            {
                Console.WriteLine("Please enter a starting date value: ");
                validDate = DateTime.TryParse(Console.ReadLine(), out startDate);
            }

            validDate = false;
            DateTime endDate = default;
            while (!validDate || startDate > endDate)
            {
                Console.WriteLine("Please enter an ending date value:");
                validDate = DateTime.TryParse(Console.ReadLine(), out endDate);
            }

            var results = new List<Result>();

            try
            {
                foreach (string file in Directory.GetFiles(@"..\..\Content\" + author, "*.json"))
                {
                    var reader = new StreamReader(file);

                    var articleInfo = JsonConvert.DeserializeObject<Article>(reader.ReadToEnd());

                    if (articleInfo.datePublished >= startDate && articleInfo.datePublished <= endDate)
                    {
                        results.Add(new Result
                        {
                            Title = articleInfo.title,
                            Date = articleInfo.datePublished.ToString(),
                            Path = file.Replace(".json", ".txt")
                        });
                    }
                }

                if (results.Count == 0)
                {
                    Console.WriteLine("No results found. Please try another date range.");
                }
                else
                {
                    var outputPath = @"..\..\Results\Result_" + DateTime.Now.ToString("yyyy-MM-ddTHHmmss") + ".json";

                    var json = JsonConvert.SerializeObject(results);

                    File.WriteAllText(outputPath, json);

                    Console.WriteLine("Result file generated at " + outputPath);
                }
            }
            catch
            {
                Console.WriteLine("No articles by the author with the name '" + author + "' were found.");
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
