﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UA.CSharp.Lab4.Main
{
    public class Article
    {
        public string title { get; set; }

        public DateTime datePublished { get; set; }

        public string[] subjectTags { get; set; }
    }
}
